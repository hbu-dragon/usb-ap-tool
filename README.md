# usbAPTool

## usbAPTool是什么

usbAPTool是一款使用USB无线网卡开启WiFi热点的工具，用来代替无线网卡自带的管理软件。它的设计初衷是为了方便OpenHarmony（开源鸿蒙）的开发者或学习者，快速建立嵌入式系统开发的网络环境。

## 为何不用无线网卡自带的管理软件

市面上主流的无线网卡，如360、水星、腾达、TP-LINK等，虽然自带管理软件，也能够建立热点，但是都存在一些共性的问题，诸如：

- 设置繁琐；
- 兼容性差；
- 稳定性差；
- 开发板接入热点后，和PC机无法相互访问；
- 开发板和手机接入热点后，无法相互访问。

这些问题给嵌入式系统开发带来了各种不便。

## usbAPTool的优点

- 设置简单快捷；
- 兼容性好；
- 不改变机房网络拓扑结构；
- 每个开发机位拥有独立的网络环境；
- 支持开发板和PC机相互访问；
- 支持开发板和手机相互访问；
- 开发板可以访问公网（开发板-USB无线网卡-PC机-校园网-公网）；
- 满足机房大规模部署要求（AP负载小、构建和移除简单快捷）；
- 同时适合移动场景（自带电脑出差、接入校园网、比赛展示）。

## usbAPTool的适用场景

usbAPTool特别适合于机房大规模部署，和移动开发场景。它主要适用于：

- 高校（或职业院校、中小学等）机房、实验室，建立嵌入式系统开发的教学网络环境和开发网络环境；
- 开发人员进行移动开发、移动测试、移动展示等场景，例如自带电脑出差、接入校园网、比赛展示的时候，需要建立网络环境；
- 普通用户不喜欢无线网卡自带的管理软件，作为替代品。

## 对USB无线网卡的要求

USB无线网卡驱动必须支持承载网络。核实方法如下：

### 安装驱动

插入无线网卡，运行驱动安装程序

### 验证承载网络支持

1. 打开命令提示符（管理员）

2. 执行命令：netsh wlan show drivers

3. 查看命令输出。如果显示“支持的承载网络：是”，说明您的USB无线网卡驱动支持承载网络。否则请重新安装驱动，或者更换其它型号或品牌的USB无线网卡。

   ![](https://gitee.com/hbu-dragon/usb-ap-tool/raw/master/README.assets/driver.png)

## usbAPTool的使用方法

1. 下载"WiFi热点工具.cmd"

   克隆本仓或者单独下载"WiFi热点工具.cmd"文件。

2. 基本参数配置（可选步骤）

   使用编辑软件打开"WiFi热点工具.cmd"。

   根据使用场景，设置SCENE。

   ![](https://gitee.com/hbu-dragon/usb-ap-tool/raw/master/README.assets/param.png)

   如果SCENE=indi，即个人网络环境，那么可以进一步自定义INDI_SSID（热点名称）和INDI_PASSWORD（热点密码），也可以用默认值。

   如果SCENE=lab，即学校机房、实验室等教学场景，那么可以进一步设置STUDENTID（学生的学号）。在lab场景下，学生的学号将作为热点名称（SSID）和密码。如果不设置STUDENTID，那么在usbAPTool启动后，将会要求学生输入学号。

3. 插入无线网卡

   如果使用PC机，建议插入到机箱前面板。

4. 运行"WiFi热点工具.cmd"

   双击"WiFi热点工具.cmd"，会要求管理员权限，请点击“是”以允许。

   工具的主界面如下：

   ![](https://gitee.com/hbu-dragon/usb-ap-tool/raw/master/README.assets/ui.png)

   直接按回车键即可自动开启热点。

   每个命令左侧有数字，输入对应数字并按回车键，可以执行相应的命令。常用命令有3（查看热点状态）和4（查看热点设置）。


## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

