@REM Copyright (c) 2022, Dragon. HeBei University. email:dragon@hbu.edu.cn
@REM The original code comes from the Internet, the original author Ruoshui.
@REM 
@REM Redistribution and use in source and binary forms, with or without
@REM modification, are permitted provided that the following conditions are met:
@REM 
@REM 1. Redistributions of source code must retain the above copyright notice, this
@REM    list of conditions and the following disclaimer.
@REM 
@REM 2. Redistributions in binary form must reproduce the above copyright notice,
@REM    this list of conditions and the following disclaimer in the documentation
@REM    and/or other materials provided with the distribution.
@REM 
@REM 3. Neither the name of the copyright holder nor the names of its
@REM    contributors may be used to endorse or promote products derived from
@REM    this software without specific prior written permission.
@REM 
@REM THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
@REM AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
@REM IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
@REM DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
@REM FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
@REM DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
@REM SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
@REM CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
@REM OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
@REM OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@ECHO OFF

set PRODUCT=WiFi热点工具
set VERSION=v2022.03.14
set AUTHOR=Dragon

@REM 使用场景。
@REM lab: 学校机房、实验室等教学场景。
@REM indi: 个人网络环境。
set SCENE=indi

@REM 学生学号。在lab场景下，作为热点名称（SSID）和密码。
@REM 学号最短8位，最长16位。不符合要求将无法成功建立热点。
@REM 留空则需要使用学号登录。
set STUDENTID=""

@REM 个人网络环境的默认ssid和key。
@REM ssid最短2位，key长度8-16位。不符合要求将无法成功建立热点。
set INDI_SSID=ohdev
set INDI_PASSWORD=openharmony

Title %PRODUCT% %VERSION% %AUTHOR%
setlocal ENABLEDELAYEDEXPANSION
mode con: cols=65 lines=28
color 07

@REM 批处理获取管理员权限
:-------------------------------------  
%1 mshta vbscript:createobject("shell.application").shellexecute("""%~0""","::",,"runas",1)(window.close)&&exit /b
    pushd "%CD%"
    CD /D "%~dp0"
:-------------------------------------- 

@REM lab场景学生登录处理
if "%SCENE%"=="lab" (
    @REM 没有设置默认学号，则提示输入
    if %STUDENTID%=="" (
        echo 您的学号将作为热点的默认名称和密码
        echo.
        :STUDENT_LOGIN
        Set /p STUDENTID=请输入学号，按[Enter]确定：
        if !STUDENTID!=="" GOTO STUDENT_LOGIN
        if "!STUDENTID:~7,1!"=="" (
            set STUDENTID=""
            echo 最少输入8位
            GOTO STUDENT_LOGIN
        )
        if "!STUDENTID:~16,1!" neq "" (
            set STUDENTID=""
            echo 最多输入16位
            GOTO STUDENT_LOGIN
        )
    )
)

set selcet=1
GOTO ICSvbs
:MENU
cls
ECHO.
ECHO.%PRODUCT% %VERSION%
ECHO.-------------------------
ECHO. [1] 开启热点（自动）
ECHO. [2] 开启热点（手动）
ECHO. [3] 查看热点状态
ECHO. [4] 查看热点设置
ECHO. [5] 更改热点设置
ECHO. [6] 更改适配器选项
ECHO. [7] 关闭热点
ECHO. [0] 退  出
ECHO.-------------------------
ECHO.
Set /p selcet=请输入数字并按[Enter]确定（默认：%selcet%）：

SET selcet=%selcet:~0,1%
If "%selcet%"=="1" set "auto=true" && Goto wifi_start
If "%selcet%"=="2" set "auto=false" && Goto wifi_start
If "%selcet%"=="3" Goto wifi_status
If "%selcet%"=="4" Goto wifi_show_config
If "%selcet%"=="5" Goto wifi_config
If "%selcet%"=="6" set "selcet=3" && ncpa.cpl
If "%selcet%"=="7" Goto wifi_stop
If "%selcet%"=="0" Goto exit

GOTO MENU

:wifi_start
set selcet=3
cls
ECHO. ---------开启无线承载网络---------
echo.
echo 1.检测Internet访问
set lost=false
set pingurl=114.114.114.114
@REM 数据包: 已发送 = 3，已接收 = 0，丢失 = 3 (100% 丢失)，
FOR /F "usebackq tokens=1 delims= " %%i in ( `ping -n 3 -w 999 %pingurl% ^| find "(100%"` ) do (
    if not %%i=="" ( set lost=true
    )
)
if %lost%==true (
    echo    无Internet访问。请连接Internet，或者完成上网认证。
    ping -n 5 -w 999 127.0.0.1 >nul 2>nul
    GOTO wifi_start
)

ECHO.
echo 2.检测承载网络支持
set Support=false
@REM 支持的承载网络  : 是
FOR /F "usebackq tokens=1,2,3 delims= " %%i in ( `netsh wlan show drivers ^| find "支持的承载网络"` ) do (
    if "%%k"=="否" ( echo    发现一块无线网卡，不支持承载网络！
    )
    if "%%k"=="是" ( echo    发现一块无线网卡，支持承载网络！
        set Support=true
    )
)
if %Support%==false ( echo    没有支持承载网络的无线网卡！
    echo    请插入一块支持承载网络的无线网卡，或者更换驱动。
    ECHO.
    pause
    GOTO MENU
)

ECHO.
echo 3.开启必要服务
FOR /D %%i in ( "icssvc" "ALG" "BFE" "MpsSvc" "LanmanServer" "LanmanWorkstation" "Browser" "Dhcp" "RpcLocator" "Dnscache" "FDResPub" "upnphost" "SSDPSRV" "lmhosts") do (
    set "Adapter=%%i"   
    @REM lab场景
    if "%SCENE%"=="lab" (
        @REM 将服务设置为自动启动。这会增加系统负载，但是对于lab场景来说，是必要的。
        @REM lab场景通常采用保护卡或者无盘站等方式，将服务设置为自动启动，电脑重启后会还原。
        @REM lab场景如果某些服务是被禁用的，需要通过设置为自动启动，来启动这些服务。这可以提高管理效率。
        sc config !Adapter! start= AUTO>nul
    )
    sc start !Adapter!>nul
)

:wan_config_auto
@REM 停止承载网络，关闭承载网络模式
netsh wlan stop hostednetwork>nul
netsh wlan set hostednetwork mode=disallow>nul
ping -n 3 -w 999 127.0.0.1 >nul 2>nul
if %auto%==false goto wan_config

ECHO.
echo 4.查询Internet网络接口
set WAN=""
set lost=""
set /a n=1
:wan_config_auto_re
FOR /D %%i in ("以太网适配器" "无线局域网适配器" "PPP 适配器") do (
    set "Adapter=%%i"
    if %%i=="PPP 适配器" (
        FOR /F "usebackq tokens=2* delims= " %%j in (`ipconfig^|find !Adapter!`) do (
            set "AdapterName=%%k"
            FOR /F "usebackq tokens=3 delims= " %%l in (`netsh interface ipv4 show address "!AdapterName:~0,-1!"^|find "IP"`) do (
            FOR /F "usebackq tokens=* delims=" %%m in (`ping -n 3 -w 999 -S %%l %pingurl%^|find "往返"`) do ( set "lost=%%m")
                if not !lost!=="" (
                    set WAN="!AdapterName:~0,-1!"
                    echo    发现：!AdapterName:~0,-1!
                    goto startap
                )
            )
        )
    ) else (
        FOR /F "usebackq tokens=1* delims= " %%j in (`ipconfig^|find !Adapter!`) do (
            set "AdapterName=%%k"
            FOR /F "usebackq tokens=3 delims= " %%l in (`netsh interface ipv4 show address "!AdapterName:~0,-1!"^|find "IP"`) do (
            FOR /F "usebackq tokens=* delims=" %%m in (`ping -n 3 -w 999 -S %%l %pingurl%^|find "往返"`) do ( set "lost=%%m")
                if not !lost!=="" (
                    set WAN="!AdapterName:~0,-1!"
                    echo    发现：!AdapterName:~0,-1!
                    goto startap
                )
            )
        )
    )
)

echo    第%n%次未找到
if %WAN%=="" (
    if %n%==4 (
        set auto=false
        echo    没有找到联网的网络接口，请尝试手动指定。
        ping -n 3 -w 999 127.0.0.1 >nul 2>nul
        GOTO wifi_start
    ) else (
        set /a n=%n%+1
        GOTO wan_config_auto_re
    )
)

:wan_config
echo 4.选择Internet网络接口
echo    确认请输入y，否则直接按[Enter]键。
ECHO.
:wan_config_re
set WAN=""
FOR /F "usebackq tokens=2* delims= " %%i in ( `ipconfig^|find "PPP 适配器"`  ) do (
    set WANt=%%j
    set /p a=是否选择“!WANt:~0,-1!”: 
    if !a!==y ( set WAN="!WANt:~0,-1!"
        goto startap
    )
)
FOR /F "usebackq tokens=1* delims= " %%i in ( `ipconfig^|find "以太网适配器"`  ) do (
    set WANt=%%j
    set /p a=是否选择“!WANt:~0,-1!”:  
    if !a!==y ( set WAN="!WANt:~0,-1!"
        goto startap
    )
)
FOR /F "usebackq tokens=1* delims= " %%i in ( `ipconfig^|find "无线局域网适配器"`  ) do (
    set WANt=%%j
    set /p a=是否选择“!WANt:~0,-1!”:  
    if !a!==y ( set WAN="!WANt:~0,-1!"
        goto startap
    )
)
if %WAN%=="" ( goto wan_config_re )

:startap
ECHO.
echo 5.启动无线承载网络
ECHO.
@REM lab场景，热点的ssid不能相同。使用学号作为默认的热点名称（SSID）和密码。可自定义。
if %SCENE%==lab ( netsh wlan set hostednetwork mode=allow ssid=%STUDENTID% key=%STUDENTID% >nul 
)
@REM indi场景，加默认ssid和key。可自定义，用于提高工作效率。
if %SCENE%==indi ( netsh wlan set hostednetwork mode=allow ssid=%INDI_SSID% key=%INDI_PASSWORD% >nul 
)
netsh wlan start hostednetwork >nul 2>nul
if not "%errorlevel%"=="0" (
    FOR /D %%i in ( "无线网络连接" "WLAN" ) do (
        set "Adapter=%%i"
        echo    开启失败，尝试重置无线网卡，以便顺利开启热点。
        FOR /F "usebackq tokens=1 delims=," %%i in ( `getmac /v /fo csv^|find !Adapter!` ) do ( 
                netsh interface set interface %%i disabled>nul 2>nul
                netsh interface set interface %%i enabled>nul 2>nul
        )
    )
    goto wifi_start
)

echo 6.查询Microsoft承载网络接口
set LAN=""
FOR /F "usebackq tokens=1 delims=," %%i in ( `getmac /v /fo csv^|find "Microsoft"` ) do (   
    set LAN=%%i
)
echo    发现：%LAN%

ECHO.
echo 7.设置Internet连接共享：%WAN%---^>%LAN%
netsh interface ip set address name=%LAN% source=DHCP>nul
netsh interface ip set dnsservers name=%LAN% source=DHCP>nul
cscript /nologo %temp%\ICS.vbs "off"
for /f "delims=" %%i in ('cscript /nologo %temp%\ICS.vbs %WAN% %LAN%') do echo %%i

ECHO.
echo 8.验证Internet连接共享
echo    等待设置生效，5秒后开始验证...
ping -n 5 -w 999 127.0.0.1 >nul 2>nul
set lost=""
FOR /F "usebackq tokens=3 delims= " %%i in (`netsh interface ipv4 show address %LAN%^|find "IP"`) do (
    FOR /F "usebackq tokens=* delims=" %%j in (`ping -n 3 -w 999 -S %%i %pingurl%^|find "往返"`) do set "lost=%%j"
)
if %lost%=="" (
    echo    Internet连接共享设置失败！
    ECHO.
    echo    尝试关闭无线承载网络，并再次开启。
    echo    关闭ICS共享...
    cscript /nologo %temp%\ICS.vbs "off"
    ECHO.
    netsh wlan stop hostednetwork
    netsh wlan set hostednetwork mode=disallow
    goto wifi_start
) else (
    echo    Internet连接共享设置成功！)
ECHO.
pause
GOTO MENU

:wifi_status
cls
set selcet=3
ECHO. ---------无线承载网络状态---------
ECHO.
echo 承载网络状态
echo ---------------------
@REM netsh wlan show hostednetwork|find "模式"
FOR /F "usebackq tokens=1* delims= " %%i in (`netsh wlan show hostednetwork^|find "模式"`) do echo   %%i %%j
@REM netsh wlan show hostednetwork|find " 状态"
FOR /F "usebackq tokens=1* delims= " %%i in (`netsh wlan show hostednetwork^|find " 状态"`) do echo   %%i %%j
@REM netsh wlan show hostednetwork|find " 客户端数"
FOR /F "usebackq tokens=1* delims= " %%i in (`netsh wlan show hostednetwork^|find " 客户端数"`) do echo   %%i %%j
ECHO.
echo 已连接客户端
echo -----------------------------------------
set "ip=---------------"
set "mac=-----------------"
FOR /F "usebackq tokens=1-6 delims=: " %%a in ( `netsh wlan show hostednetwork ^| find "已经过身份验证"` ) do (
    set "mac=%%a-%%b-%%c-%%d-%%e-%%f"
    FOR /F "usebackq tokens=1 delims=: " %%i in ( `ARP -A^|find "!mac!"` ) do (
        set "ip=%%i"
    )
)
echo   IP地址		MAC地址
echo   %ip%	%mac%
ECHO.
ECHO.
pause
GOTO MENU

:wifi_stop
cls
set selcet=0
ECHO. ---------关闭无线承载网络---------
ECHO.
echo 关闭ICS共享
cscript /nologo %temp%\ICS.vbs "off"
ECHO.
netsh wlan stop hostednetwork
netsh wlan set hostednetwork mode=disallow
pause
GOTO MENU

:wifi_config
cls
set selcet=1
ECHO. ---------配置无线承载网络---------
ECHO.
echo 第一步：输入新SSID [Enter]结束
echo.
:begin
set SSID=""
set /p SSID=请输入新SSID：
if %SSID%=="" goto begin
if "%SSID:~1,1%"==""  echo 最少输入2位&goto begin
echo.
echo.
echo 第二步：输入8位以上新密码 [Enter]结束
echo.
:key
set pw=""
set /p pw=请输入密码(8-16位)：
if %pw%=="" goto key
if "%pw:~7,1%"==""  echo 最少输入8位&goto key
if "%pw:~16,1%" neq "" echo 最多输入16位&goto key

netsh wlan set hostednetwork ssid=%SSID% key=%pw%
pause
GOTO MENU

:wifi_show_config
cls
set selcet=3
ECHO. ---------无线承载网络设置---------
ECHO.
echo 承载网络设置
echo ----------------------------
@REM netsh wlan show hostednetwork|find "名称"
FOR /F "usebackq tokens=2* delims=:" %%i in (`netsh wlan show hostednetwork^|find "名称"`) do echo SSID名称	%%i
@REM netsh wlan show hostednetwork setting=security|find "用户安全密钥 "
FOR /F "usebackq tokens=2* delims=:" %%i in (`netsh wlan show hostednetwork security^|find "用户安全密钥 "`) do echo 用户安全密钥	%%i
@REM netsh wlan show hostednetwork setting=security|find "身份验证"
FOR /F "usebackq tokens=2* delims=:" %%i in (`netsh wlan show hostednetwork security^|find "身份验证"`) do echo 身份验证	%%i
@REM netsh wlan show hostednetwork|find "最多客户端数"
FOR /F "usebackq tokens=2* delims=:" %%i in (`netsh wlan show hostednetwork^|find "最多客户端数"`) do echo 最多客户端数	%%i
ECHO.
pause
GOTO MENU

:ICSvbs
if exist "%temp%\ICS.vbs" ( del /f /a /q "%temp%\ICS.vbs" )
(
    echo '使用方法
    echo 'cscript /nologo %%^~dp0\ics.vbs "本地连接" "无线网络连接 2"
    echo 'cscript /nologo %%^~dp0\ics.vbs "off"
    echo Option Explicit
    echo Dim ICSSC_DEFAULT,CONNECTION_PUBLIC,CONNECTION_PRIVATE,CONNECTION_ALL
    echo Dim NetSharingManager,PublicConnection, PrivateConnection,EveryConnectionCollection
    echo ICSSC_DEFAULT      = 0
    echo CONNECTION_PUBLIC  = 0     '公有网络接口
    echo CONNECTION_PRIVATE = 1     '私有网络接口
    echo CONNECTION_ALL     = 2     '全部网络接口
    echo Dim objArgs
    echo Dim priv_con,publ_con,switch
    echo Dim Reinfo
    echo Main^(^)
    echo Wscript.quit
    echo '主函数
    echo Function Main^(^)
    echo     Dim bReturn
    echo     bReturn = "false"
    echo    Set objArgs = Wscript.Arguments
    echo    If objArgs.Count = 2 Then
    echo        publ_con = objArgs^(0^)
    echo        priv_con = objArgs^(1^)
    echo        switch = "on"
    echo        ICSshare publ_con,priv_con,switch
    echo    ElseIf  objArgs.Count = 1 Then
    echo        switch = objArgs^(0^)
    echo        ICSshare bReturn,bReturn,switch
    echo    End If
    echo    Main = bReturn
    echo End Function
    echo Private Function ICSshare^(ByVal sPublic, ByVal sPrivate, ByVal bEnable^)
    echo     On Error Resume Next
    echo     Dim NetSharingManager,EveryConnectionCollection, Item, EveryConnection, objNCProps
    echo     set NetSharingManager = Wscript.CreateObject^("HNetCfg.HNetShare.1"^)
    echo    set EveryConnectionCollection = NetSharingManager.EnumEveryConnection
    echo     for each Item in EveryConnectionCollection
    echo         set EveryConnection = NetSharingManager.INetSharingConfigurationForINetConnection^(Item^)
    echo         set objNCProps = NetSharingManager.NetConnectionProps^(Item^)
    echo        If objNCProps.name = sPublic And bEnable = "on" Then
    echo            EveryConnection.EnableSharing CONNECTION_PUBLIC
    echo            WSH.echo "    已设置WAN接口：“" ^& objNCProps.name ^& "”"
    echo         ElseIf objNCProps.name = sPrivate And bEnable = "on" Then
    echo            EveryConnection.EnableSharing CONNECTION_PRIVATE
    echo            WSH.echo "    已设置LAN接口：“" ^& objNCProps.name ^& "”"
    echo        ElseIf bEnable = "on" Then
    echo            EveryConnection.EnableSharing CONNECTION_ALL
    echo        ElseIf bEnable = "off" Then
    echo             EveryConnection.DisableSharing
    echo         End If
    echo     Next
    echo End Function
)>%temp%\ICS.vbs
GOTO MENU

:Exit
cls
del /f /a /q "%temp%\ICS.vbs"